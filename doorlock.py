#!/usr/bin/env python

try:
    import RPi.GPIO as GPIO
    import time
    import fdb
    import signal
    import sys
    import evdev
    import mysql.connector
	import syslog
    from evdev  import InputDevice, categorize, ecodes
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")

con = ""
mydb = ""
dev = InputDevice('/dev/input/event0')


scancodes = {
    # Scancode: ASCIICode
    0: None, 1: u'ESC', 2: u'1', 3: u'2', 4: u'3', 5: u'4', 6: u'5', 7: u'6', 8: u'7', 9: u'8',
    10: u'9', 11: u'0', 12: u'-', 13: u'=', 14: u'BKSP', 15: u'TAB', 16: u'Q', 17: u'W', 18: u'E', 19: u'R',
    20: u'T', 21: u'Y', 22: u'U', 23: u'I', 24: u'O', 25: u'P', 26: u'[', 27: u']', 28: u'CRLF', 29: u'LCTRL',
    30: u'A', 31: u'S', 32: u'D', 33: u'F', 34: u'G', 35: u'H', 36: u'J', 37: u'K', 38: u'L', 39: u';',
    40: u'"', 41: u'`', 42: u'LSHFT', 43: u'\\', 44: u'Z', 45: u'X', 46: u'C', 47: u'V', 48: u'B', 49: u'N',
    50: u'M', 51: u',', 52: u'.', 53: u'/', 54: u'RSHFT', 56: u'LALT', 100: u'RALT'
}


def signal_handler(sig, frame):
	print('You pressed CTRL+C!')
	sys.exit(0)

def setGPIO():
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(2, GPIO.OUT, initial=GPIO.HIGH)

def dbConnect():
	global con
	global mydb
#The ServerConnection
	try:
		con = fdb.connect(
				host='DESKTOP-ULTS9AS', database='D:\TEST_SERVER.FDB',
				user='sysdba', password='masterkey')
		syslog.syslog(syslog.LOG_NOTICE, "Firebird DB connected" )
		print (type(con))
	except fdb.Error:
		syslog.syslog(syslog.LOG_ERR, "Error connecting Firebird DB")


	try:
		mydb = mysql.connector.connect(
			host = "my.url",
			user = "whatever",
			passwd = "password",
			database = "doorlock" )
		syslog.syslog(syslog.LOG_NOTICE, "MYSQL connected")
	except mysql.connector.Error as err:
		syslog.syslog(syslog.LOG_ERR, "Error connecting MYSQL: {}".format(err))


def checkAccess(tag_id):
	global con
	val = "" 
 	GPIO.output(2, GPIO.HIGH)
 	update_entry = -1
 	tid = 0
	signal.signal(signal.SIGINT, signal_handler)
	select = "select * from test where tag_id = '%s'" %tag_id
	try:
		cur = con.cursor()
		cur.execute(select)
	except fdb.Error:
		syslog.syslog(syslog.LOG_ERR, "Database error")
		time.sleep(30)
		dbConnect()
		checkAccess(tag_id)
		return
	except AttributeError:
		syslog.syslog(syslog.LOG_ERR, "Datebase error")
		time.sleep(30)
		dbConnect()
		checkAccess(tag_id)
		return

	try:
		for (name, sn, tag_id,remaining_entry) in cur:
			print 'Tag: %s, Kunde: %s, %s Eintritt verbleibend: %s' %(tag_id, sn, name, remaining_entry)
			if remaining_entry > 0:
				syslog.syslog(syslog.LOG_NOTICE,'\033[92m' + "unlocking door..." + '\033[0m')
				GPIO.output(2, GPIO.LOW)
				time.sleep(3)
				GPIO.output(2,GPIO.HIGH)
				update_entry = remaining_entry - 1 
				tid = tag_id
				val = ( tag_id, sn, name, int(time.time()) )
			else:
		 		syslog.syslog(syslog.LOG_NOTICE,'\033[91m' + "nope..." + '\033[0m')
				return
	except fdb.Error:
		syslog.syslog(syslog.LOG_NOTICE, "nothing found")
		return

	if  update_entry >= 0:
	    mycursor = mydb.cursor()
        insert_log = "INSERT INTO entry_log (TAG_ID, NAME, SN, ENTRY_TS) VALUES (%s, %s, %s, %s)"
	    mycursor.execute(insert_log, val)
	    mydb.commit()
	    print (mycursor.rowcount, "records inserted") 
	    tid = 0
	    update_entry = 0
	

tag_id = ""
doScan = True
dbConnect()
setGPIO()

for event in dev.read_loop():
     if event.type == ecodes.EV_KEY:
        data = evdev.categorize(event)
        if data.keystate == 1 and doScan == True: #Down only
                key_lookup = scancodes.get(data.scancode)
                if key_lookup != 'CRLF':
                        tag_id = tag_id + key_lookup
                else:
                        doScan = False
                        checkAccess(tag_id)
			doScan = True
                        tag_id = ""


